package com.gmo.tools;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class TimeDiffByLong {
	
	/**
	 * @param date
	 * @param timeLong
	 * @return
	 * @throws ParseException 
	 */
	public static boolean compareDate(String date, String timeLong) throws ParseException{
		
		SimpleDateFormat format = new SimpleDateFormat(
                "dd/MMM/yyyy:HH:mm:ss Z");
		
		Timestamp timestampLong = new Timestamp(Long.parseLong(timeLong));
		
		Timestamp timeDate = new Timestamp(format.parse(date).getTime());
		
		return timeDate.after(timestampLong);
		
	}
	
    public static void main( String[] args )
    {
        System.out.println( "Tools!" );
    }
}
